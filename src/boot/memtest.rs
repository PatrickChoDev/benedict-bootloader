use uefi::prelude::*;
use uefi::table::boot::{AllocateType, BootServices, MemoryDescriptor, MemoryType};

use crate::alloc::vec::Vec;
use core::mem;

pub fn test(bt: &BootServices) {
    info!("Testing memory functions");
    allocate_pages(bt);
    vec_alloc();
    alloc_alignment();
    memmove(bt);
    memory_map(bt);
}

fn allocate_pages(bt: &BootServices) {
    info!("Allocating some pages of memory");
    let ty = AllocateType::AnyPages;
    let mem_ty = MemoryType::LOADER_DATA;
    let pgs = bt
        .allocate_pages(ty, mem_ty, 1)
        .expect_success("Failed to allocate a page of memory");
    assert_eq!(pgs % 4096, 0, "Page pointer is not page-aligned");
    let buf = unsafe { &mut *(pgs as *mut [u8; 4096]) };
    buf[0] = 0xF0;
    buf[4095] = 0x23;
    bt.free_pages(pgs, 1).unwrap_success();
    info!("Allocate page passed!");
}

fn vec_alloc() {
    info!("Allocating a vector through the `alloc` crate");
    let mut values = vec![-5, 16, 23, 4, 0];
    values.sort_unstable();
    assert_eq!(values[..], [-5, 0, 4, 16, 23], "Failed to sort vector");
    info!("Vector Allocate passed!");
}

fn alloc_alignment() {
    info!("Allocating a structure with alignment to 0x100");
    #[repr(align(0x100))]
    struct Block([u8; 0x100]);
    let value = vec![Block([1; 0x100])];
    assert_eq!(value.as_ptr() as usize % 0x100, 0, "Wrong alignment");
    info!("Alignment passed!");
}

fn memmove(bt: &BootServices) {
    info!("Testing the `memmove` / `set_mem` functions");
    let src = [1, 2, 3, 4];
    let mut dest = [0u8; 4];
    unsafe {
        bt.set_mem(dest.as_mut_ptr(), dest.len(), 1);
    }
    assert_eq!(dest, [1; 4], "Failed to set memory");
    unsafe {
        bt.memmove(dest.as_mut_ptr(), src.as_ptr(), dest.len());
    }
    assert_eq!(dest, src, "Failed to copy memory");
    info!("Memory move passed!");
}

fn memory_map(bt: &BootServices) {
    info!("Testing memory map functions");
    let map_sz = bt.memory_map_size();
    let buf_sz = map_sz + 8 * mem::size_of::<MemoryDescriptor>();
    let mut buffer = Vec::with_capacity(buf_sz);
    unsafe {
        buffer.set_len(buf_sz);
    }
    let (_key, desc_iter) = bt
        .memory_map(&mut buffer)
        .expect_success("Failed to retrieve UEFI memory map");
    let descriptors = desc_iter.copied().collect::<Vec<_>>();
    assert!(!descriptors.is_empty(), "Memory map is empty");
    let first_desc = descriptors[0];
    #[cfg(target_arch = "x86_64")]
    {
        let phys_start = first_desc.phys_start;
        assert_eq!(phys_start, 0, "Memory does not start at address 0");
    }
    let page_count = first_desc.page_count;
    assert!(page_count != 0, "Memory map entry has zero size");
    info!("Memory map passed!");
}
