use uefi::table::boot::BootServices;
mod memtest;
pub fn test(bt: &BootServices) {
    info!("Testing boot services");
    memtest::test(bt);
}
