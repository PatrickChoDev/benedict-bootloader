#![allow(non_snake_case)]
#![no_std]
#![no_main]
#![feature(asm)]
#![feature(abi_efiapi)]

#[macro_use]
extern crate alloc;
#[macro_use]
extern crate log;

mod boot;
use alloc::string::String;
use core::mem;
use uefi::prelude::*;
use uefi::table::boot::MemoryDescriptor;

#[entry]
fn uefi_start(_image_handler: uefi::Handle, mut sys_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut sys_table).expect_success("Failed to initialize utils");
    sys_table
        .stdout()
        .reset(false)
        .expect_success("Failed to reset output buffer");

    let rev = sys_table.uefi_revision();
    info!("UEFI Version {}.{}", rev.major(), rev.minor());

    let mut buf = String::new();
    sys_table.firmware_vendor().as_str_in_buf(&mut buf).unwrap();
    info!("Firmware Vendor: {}", buf.as_str());
    sys_table
        .stdout()
        .reset(false)
        .expect_success("Failed to reset stdout");

    check_revision(sys_table.uefi_revision());
    let bt = sys_table.boot_services();
    boot::test(bt);
    // shutdown(_image_handler, sys_table);
    Status::SUCCESS
}

fn check_revision(rev: uefi::table::Revision) {
    let (major, minor) = (rev.major(), rev.minor());

    info!("UEFI {}.{}", major, minor / 10);

    assert!(major >= 2, "Running on an old, unsupported version of UEFI");
    assert!(
        minor >= 30,
        "Old version of UEFI 2, some features might not be available."
    );
}

fn shutdown(image: uefi::Handle, st: SystemTable<Boot>) -> ! {
    use uefi::table::runtime::ResetType;
    let max_mmap_size =
        st.boot_services().memory_map_size() + 8 * mem::size_of::<MemoryDescriptor>();
    let mut mmap_storage = vec![0; max_mmap_size].into_boxed_slice();
    let (st, _iter) = st
        .exit_boot_services(image, &mut mmap_storage[..])
        .expect_success("Failed to exit boot services");
    let rt = unsafe { st.runtime_services() };
    rt.reset(ResetType::Shutdown, Status::SUCCESS, None);
}
