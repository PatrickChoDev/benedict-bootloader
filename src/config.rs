use log::{debug, info};
use uefi::proto::media::file;

pub fn read_config() -> () {
    debug!("Loading Configuration files...");
    let mut f = file::File::open(
        "hi.txt",
        file::FileMode::Read,
        file::FileAttribute::READ_ONLY,
    );

    info!("Nice!!")
}
