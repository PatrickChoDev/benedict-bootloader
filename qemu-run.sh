#!/bin/sh

OVMF_CODE='build/OVMF_CODE.fd'
OVMF_VARS='build/OVMF_VARS-1024x768.fd'
BUILD_DIR=$(pwd)/target/x86_64-unknown-uefi/debug

mkdir -p ./build/EFI/BOOT
cp $BUILD_DIR/Benedict.efi ./build/EFI/BOOT/BOOTx64.efi

sudo qemu-system-x86_64 \
    -nodefaults \
    -machine q35,accel=kvm:tcg \
    -m 128M \
    -drive if=pflash,format=raw,readonly=on,file=$OVMF_CODE \
    -drive if=pflash,format=raw,file=$OVMF_VARS \
    -drive format=raw,file=fat:rw:./build \
    -serial /dev/null \
    -vga std
