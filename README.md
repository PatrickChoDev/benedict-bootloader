# Benedict Bootloader

**Benedict** is a `x86_64` and `arrch64` written in Rust.

### **Features**

- [x] Support FS
- [ ] Support PXE boot
- [ ] Support EFI custom shell
- [ ] Support secure-boot
